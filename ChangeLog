2011-12-17  Jeffrey Stedfast  <fejj@gnome.org>

	* src/main.c: Removed GnomeProgram API usage.

	* src/alleyoop.c: Subclass GtkWindow instead of GnomeApp and
	updated to use GtkUIManager APIs to replace GnomeUIInfo APIs.

2011-12-17  Jeffrey Stedfast  <fejj@gnome.org>

	* alleyoop.desktop.in: Added.

	* src/alleyoop.c: Replaced the GnomeAbout dialog with a
	GtkAboutDialog.

	* src/vggeneralprefs.c: Replace GnomeFileEntry with
	GtkFileChooserButton.

2011-12-17  Jeffrey Stedfast  <fejj@gnome.org>

	* src/main.c: Get rid of glade dependency.

	* src/*.c: Replaced references to libgnome/gnome-i18n.h with
	glib/gi18n.h in an effort to remove libgnome(ui) dependencies.

2011-12-17  Jeffrey Stedfast  <fejj@gnome.org>

	* configure.in: Bumped version to 0.9.8

2010-12-06  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgmemcheckprefs.c: Added support for Memcheck's
	--track-origins flag.

2010-02-03  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Add -fno-strict-aliasing to CFLAGS.

2009-11-24  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: We now require glib-2.0 >= 2.14 for GRegex.

	* src/vgdefaultview.c: Switched to using GRegex which gives us
	much nicer error messages when regex patterns fail to
	compile. Plus the GRegex API is a bit nicer to work with.

2009-11-23  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.7

	* src/vgrule-list.c (vg_rule_list_add_rule): Use g_get_home_dir()
	instead of getenv("HOME") as this is probably more portable.

	* src/main.c (main): If getopts_get_args() returns 0 arguments,
	set args to NULL so that the Run command will prompt the user for
	a command to run.

2009-11-23  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.6

	* src/vggeneralprefs.c (vg_general_prefs_init): Added new widget
	to control the --track-origins Valgrind option, thanks to a patch
	by Gérald Fauvelle.
	(general_args): Change the default value of the --num-callers
	argument to 0 so that if it somehow gets set to 0, it gets ignored
	rather than passing --num-callers=0 and causing Valgrind to error
	out.

2009-11-09  Jeffrey Stedfast  <fejj@novell.com>

	* src/vggeneralprefs.c (general_prefs_get_argv): Don't bother with
	the --alignment argument anymore. Valgrind has long since been
	fixed to use a reasonable default.

2009-04-24  Jeffrey Stedfast  <fejj@novell.com>

	Patch by Petr Uzel <petr.uzel@suse.cz>

	* src/vgmemcheckprefs.c: #include stdlib.h
	(entry_focus_out): Use strcmp for key comparisons.

	* src/vghelgrindprefs.c: #include stdlib.h
	(decode_hex_value): Fixed printf hex-formatter.
	(menu_item_activated): Use strcmp for key comparison.
	(option_menu_new): Same.
	(helgrind_prefs_get_argv): Here too.

	* src/vggeneralprefs.c (general_prefs_get_argv): Use strcmp for
	key comparison rather than a pointer compare.

	* src/vgerror.c (vg_error_parser_step): Removed unused variable.

	* src/vgdefaultview.c (tree_button_press): Return FALSE from
	non-void function.

2009-03-08  Jeffrey Stedfast  <fejj@novell.com>

	* src/alleyoop.schemas: Updated.

	* src/vghelgrindprefs.c (helgrind_prefs_get_argv): Changed the
	trace-level gconf key to store an int instead of string.

2009-03-07  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.5

	* src/main.c (main): Fixed options table to use CUSTOM args so
	that our callbacks get called.

	* src/vghelgrindprefs.c: Updated for the new Helgrind
	configuration options.
	(helgrind_prefs_get_argv): Disable --trace-addr/--trace-level if
	the trace-addr string is empty.

	* src/vgmemcheckprefs.c: Add support for --ignore-ranges,
	--malloc-fill, and --free-fill.

2009-02-08  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.4

	* src/alleyoop.c: Get rid of the SymTab context - we don't
	actually want this anymore. Valgrind already gives us all we can
	hope to get from the debugging symbols for filename lookups.

	* src/vgtoolview.c (vg_tool_view_set_symtab): Removed.

	* src/vgdefaultview.c (resolve_full_path): Removed our own symtab
	lookups to try and get the source filename. If that debugging
	symbol information is available, Valgrind will give it to us, no
	need to do the same work for no added gain.

	* src/getopts.c (getopts_get_next_opt): When we get to the end of
	a group of short options, make sure to advance past any argument
	args that the short options may have consumed.

	* src/symtab.[c,h]: Removed.

	* src/ldd.[c,h]: Removed.

2007-09-15  Yannig Marchegay <yannig@marchegay.org>

        * po/oc.po: Updated Occitan translation

2007-04-05  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgdefaultview.c (view_show_error): Handle VG_STACK_EMPTY
	here too.

	* src/vgerror.c (vg_error_stack_new): Init the stack type to
	VG_STACK_EMPTY instead of VG_STACK_SOURCE so that we can better
	handle run-time generated code segments.
	(vg_error_parser_step): Handle "???" being at the end of the line
	too (e.g. not followed by a space).

2006-08-14  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.3

	* src/main.c: Reworked to use my own custom getopt parser rather
	than popt since popt sucks and using it in combination with
	GnomeProgram breaks stuff anyway.

	* src/getopts.[c,h]: A really nice option parser API.

2006-07-28  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.2

	* src/vgdefaultview.c (tree_button_press): Select the row before
	doing anything else in case the user is right-clicking or
	double-clicking a row that she did not select first.

2006-07-27  Jeffrey Stedfast  <fejj@novell.com>

	* src/alleyoop.schemas: The default value for leak-check should be
	"no", not "false".

2006-07-06  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgstrpool.c (vg_strfree): Updated to not need to malloc
	memory for each bucket in the hash table (since all we really need
	is a ref_count, we can just use that as the hash table data).

2006-06-12  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgerror.c: Updated to use the new member names.

	* src/vgerror.h (timestamp_t): Changed to reduce the size by 20
	bytes and use a union so we can have better member names.

2006-06-12  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgerror.c (decode_timestamp_and_pid): New function to
	extract the timestamp (2.4.0 and 3.1.0 formats) and pid in a log
	stream.
	(vg_error_parser_step): Use the new function to parse the
	timestamp formats to keep this function a little less messy.
	(vg_error_timestamp_to_string): New helper function to write out
	the correct timestamp format.
	(vg_error_stack_to_string): Use the timestamp formatter.
	(vg_error_summary_to_string): Same.
	(vg_error_to_string): Here too.

	* src/vgrule.c (vg_rule_type_from_report): Match against " in loss
	record " instead of " are still reachable in loss record " for
	Leak suppressions. This will match against all 3 types of Leak
	warnings.

2006-06-12  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.1 and making a release that
	works with Valgrind 3.1.0.

	* src/symtab.c (symtab_resolve_addr): Fixed some casting problems
	with some pointer arithmetic.

2006-02-28  Jeffrey Stedfast  <fejj@novell.com>

	* src/vggeneralprefs.c: #ifdef'd out the --sloppy-malloc option
	that no longer seems to be available in Valgrind 3.1.0

	* src/alleyoop.c (alleyoop_run): The Valgrind --logfile-fd option
	has changed to --log-fd in version 3.1.0 (possibly 3.0.0 too?)

2006-01-24  Clytie Siddall <clytie@riverland.net.au>

	* configure.in:	Added vi in ALL_LINGUAS line.
	
2005-08-01  Ahmad Riza H Nst  <rizahnst@eriagempita.co.id>

	* configure.in: Added id in ALL_LINGUAS line.

2005-06-09  Pauli Virtanen  <pauli.virtanen@hut.fi>

	* configure.in: Added "fi" (Finnish) in ALL_LINGUAS.

2005-06-01  Jeffrey Stedfast  <fejj@novell.com>

	* src/ldd.c (ldd_parse_linebuf): Made a little more robust.

2005-05-24  Pawan Chitrakar  <pawan@nplinux.org>

	* configure.in: Added ne in ALL_LINGUAS

2005-05-05  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: Bumped version to 0.9.0, making a new release that
	works with Valgrind 2.4.0

2005-04-08  Jeffrey Stedfast  <fejj@novell.com>

	* src/alleyoop.c (alleyoop_run): Free the entire argv array after
	fork/exec'ing.

	* src/vgmemcheckprefs.c (vg_memcheck_prefs_init): The leak-check
	option is now an option menu rather than a checkbox with the
	following values: "no", "summary", "full".

	* src/vgrule.c (vg_rule_type_from_report): Added support for
	matching against leak reports.

2005-04-08  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgerror.c (vg_error_stack_new): Return NULL (this isn't a
	void function).
	(vg_error_parser_step): handle parsing the time-stamp if it
	exists.
	(vg_error_to_string): Handle outputting the time-stamp if
	available.
	(vg_error_summary_to_string): Same.
	(vg_error_stack_to_string): Same.

	* src/vgtoolview.c (tool_view_step): Must return a value.

2005-04-07  Jeffrey Stedfast  <fejj@novell.com>

	* src/vggeneralprefs.c (vg_general_prefs_init): Add a checkbox for
	the new time-stamp feature of valgrind 2.4

2005-04-01  Steve Murphy  <murf@e-tools.com>

	* configure.in: Added "rw" to ALL_LINGUAS.

2005-02-10  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgrule-list.c (add_response_cb): Save the rules after adding
	it so that if the user never opens the rule-list widget, at least
	his suprpessions added from a right-click menu are saved.

2005-02-08  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.8.3

2005-02-08  Jeffrey Stedfast  <fejj@novell.com>

	* configure.in: check that bfd_get_section_size_before_reloc()
	exists

	* src/symtab.c (symtab_map_new): Use before_reloc() if we can
	(older binutils packages don't have the new symbol).

2005-01-31  Jeffrey Stedfast  <fejj@novell.com>

	* src/symtab.c (symtab_map_new): Use bfd_get_section_size() to
	build with newer libbfd versions.

2004-09-29  Kjartan Maraas  <kmaraas@gnome.org>

	* configure.in: Add «nb» to ALL_LINGUAS.

2004-09-09  Ankit Patel <ankit@redhat.com>

	* configure.in: Added 'gu' (Gujarati) to ALL_LINGUAS.

2004-06-25  Jeffrey Stedfast  <fejj@novell.com>

	* src/vgerror.c (vg_error_parser_step): Handle "warning: ..." log
	messages (and any additional lines that belong to said warning
	message).

2004-04-30  Adam Weinberger  <adamw@gnome.org>

	* configure.in: Added en_CA to ALL_LINGUAS.

2004-04-07 Yuriy Syrota  <rasta@cvs.gnome.org>

	* configure.in: Added "uk" (Ukrainian) to ALL_LINGUAS.

2004-04-04  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.8.2

2004-04-04  Gareth Owen  <gowen72@yahoo.com>

	* configure.in: Added en_GB to ALL_LINGUAS

2004-04-03  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vggeneralprefs.c (general_prefs_get_argv): Fixed so we don't
	end up appending NULL args to argv. Doh.

	* src/vghelgrindprefs.c (helgrind_prefs_get_argv): Same.

	* src/vgmemcheckprefs.c (memcheck_prefs_get_argv): Same.

	* src/vgcachegrindprefs.c (cachegrind_prefs_get_argv): Check for
	empty string too.

	* src/prefs.c (alleyoop_prefs_create_argv): Always specify a skin
	(valgrind 2.1.1 seems to require this whereas previous versions
	did not).

2004-04-02  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vghelgrindprefs.c (helgrind_prefs_get_argv): Same.

	* src/vgmemcheckprefs.c (memcheck_prefs_get_argv): Same as
	general_prefs_get_argv().

	* src/vgerror.c (vg_error_stack_to_string): The stack addr token
	should begin with "0x" so add that.

	* src/alleyoop.c: Re-enable the menu item for the helgrind skin.

	* src/vggeneralprefs.c (vg_general_prefs_init): Added new option
	for --track-fds (new feature in valgrind 2.1.1, whoo!)
	(general_prefs_get_argv): Only pass the value on the command-line
	if it is not the default value (this way the user can run an older
	version of valgrind (ie. one that doesn't have the latest fancy
	feature that alleyoop has a setting for) successfully.

2004-03-22  Wang Jian  <lark@linux.net.cn>

	* configure.in: Added "zh_CN" to ALL_LINGUAS.

2004-03-12  Jeffrey Stedfast  <fejj@ximian.com>

	* src/symtab.c (slurp_symtab): Use fprintf instead of g_warning.

2004-03-10 Alastair McKinstry <mckinstry@computer.org>

	* configure.in: Added "ga" to ALL_LINGUAS.

2004-03-09  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgerror.c (vg_error_parser_step): Fixed a type-o in the
	line-number parsing code.

2004-02-09  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.8.1

	* src/vgrule.c (vg_rule_type_from_report): Added code to detect
	invalid free error types as well as invalid reads of size 1,2,4,8.

	* src/vgerror.c (vg_error_pop): Restructured if/else statements to
	not need to check the same values twice.

2004-02-01  Robert Sedak  <robert.sedak@sk.htnet.hr>

	* configure.in: Added "hr" (Croatian) to ALL_LINGUAS.

2004-01-24  Marcel Telka  <marcel@telka.sk>

	* configure.in (ALL_LINGUAS): Added sk.

2004-01-13  Laurent Dhima  <laurenti@alblinux.net>

	* configure.in: Added "sq" to ALL_LINGUAS.

2004-01-08  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgdefaultview.c (view_show_error): NULL-check the filename
	string before trying to append it to the GString. Prevents a
	warning from glib.

2004-01-07  Sanlig Badral  <badral@openmn.org>

	* configure.in: Added "mn" to ALL_LINGUAS.

2004-01-03  Jeffrey Stedfast  <fejj@ximian.com>

	* src/process.c (process_fork): Oops, fixed a mistake from my last
	commit.

	* configure.in: Add a --disable-deprecated configure flag which is
	used to decide if we should define *_DISABLE_DEPRECATED in the
	build.

	* src/Makefile.am: Use the new DISABLE_DEPRECATED dingus.

2003-12-03  Jeffrey Stedfast  <fejj@ximian.com>

	* src/process.c (process_fork): Don't close all the child's fd's,
	use fcntl to set the FD_CLOEXEC attribute on them instead. Also
	start the loop at fd = 3 so we don't have to check for
	stdin/out/err.

2003-11-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/legolas.c (elf32_load): Load the .debug_str and .debug_line
	sections.
	(elf64_load): Same.

2003-11-15  Jeffrey Stedfast  <fejj@ximian.com>

	* src/legolas.c (elf32_load): load dynamic linking information.
	(elf64_load): Same.
	(elf_shared_libs_foreach): New function to get all the shared
	objects that an ELF object depends on.

2003-11-15  Jeffrey Stedfast  <fejj@ximian.com>

	* src/legolas.c: Changed the ElfSection structure to contain a
	link to a related section and also to hold its own data as there
	may be multiple strtabs and/or symtabs. ElfDesc now contains a
	pointer to the shstrtab ElfSection rather than the shstrtab data
	itself.
	(elf32_load): Upated for above design changes.
	(elf64_load): Same.
	(elf32_get_symbols): Implemented.
	(elf64_get_symbols): Implemented.
	(elf_dump): New function for debugging mostly, dumps parsed elf
	object contents to stdout.

2003-11-12  Jeffrey Stedfast  <fejj@ximian.com>

	* src/legolas.h: Make the definition of ElfSection public.

	* src/legolas.c (elf32_load): Construct a linked list of ElfSection's.
	(elf64_load): Same.
	(elf_close): Free the linked list of ElfSection's.

2003-11-10  Jeffrey Stedfast  <fejj@ximian.com>

	* src/legolas.[c,h]: New source files to decode ELF object files,
	eventually meant to replace our need for binutils' bfd.

2003-10-28  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vggeneralprefs.c (general_prefs_get_argv): If the
	suppressions file doesn't exist, don't pass the arg.

2003-10-21  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.8.0

2003-10-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgrulepattern.c (vg_rule_pattern_matches): Use
	vg_rule_type_from_report().

	* src/vgrule.c (vg_rule_type_from_report): Split out from
	vg_rule_editor_new_from_summary(). Parses a summary report string
	to try and figure out what rule-type a suppression should be in
	order to suprpess this type of error. Also parses out the syscall
	string.

	* src/vgrule-editor.c (vg_rule_editor_new_from_summary): Use
	vg_rule_type_from_report().

2003-10-20  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgrulepattern.c (vg_rule_pattern_free): Handle the case
	where @pat is NULL to be consistant with other free funcs.

	* src/vgrule.c (vg_rule_free): Free the syscall string member.

	* src/vgrule-list.c (edit_response_cb): emit the RULE_ADDED signal
	here too.

	* src/vgdefaultview.c (vg_default_view_init): init the list of
	suppressions (array of VgRulePatterns).
	(vg_default_view_finalize): free the suppression patterns.
	(recv_error_cb): suppress any errors that match any new rules the
	user has added since invocation.
	(valgrind_view_disconnect): Clear out suppression rules added this
	session.

2003-10-20  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgdefaultview.c (vg_default_view_init): Connect to the
	rule-added signal on the rule_list widget.
	(rule_added): New callback function for the rule_added
	signal. Removes all errors that match the new rule from the view.

	* src/vgrulepattern.[c,h]: New source files to construct a pattern
	matcher for a VgRule against a VgError.

	* src/vgrule-list.c (add_response_cb): Emit a rule-added event.

	* src/vgrule-editor.c (vg_rule_editor_new_from_summary): Set the
	addrcheck/memcheck checkboxes to TRUE.
	(vg_rule_editor_new): Same.

2003-09-01  Metin Amiroff  <metin@karegen.com>

	* configure.in: Added "az" in ALL_LINGUAS.

2003-08-15  Christophe Merlet  <redfox@redfoxcenter.org>

	* configure.in: Added "fr" (French) to ALL_LINGUAS.

2003-07-27  Alessio Frusciante  <algol@firenze.linux.it>

	* configure.in: Added "it" (Italian) to ALL_LINGUAS.

2003-07-26  Hasbullah Bin Pit  <sebol@ikhlas.com>

	* configure.in: Added 'ms' (Malay) in ALL_LINGUAS.

2003-07-25  Artur Flinta  <aflinta@cvs.gnome.org>

	* configure.in (ALL_LINGUAS): Added "pl" (Polish).

2003-07-09  Christian Neumair  <chris@gnome-de.org>

	* configure.in (ALL_LINGUAS): Added "de" (German).

2003-06-06  Pablo Gonzalo del Campo  <pablodc@bigfoot.com>

	* configure.in (ALL_LINGUAS): Add "es" (Spanish).

2003-06-01  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.7.3

	* src/main.c (main): Unref the program object after exiting the
	main loop.

2003-05-24  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgdefaultview.c (custom_editor_cb): Rearrange a bit for
	better error handling of the case where we cannot resolve the path
	of the source file.
	(emacs_cb): Handle not being able to resolve the src file path.
	(spawn_editor): Same.

	* src/vgerror.c (vg_error_parser_step): Modified to handle parsing
	error reports from multiple pids at once.

2003-05-21  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped the version to 0.7.2

2003-05-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/alleyoop.c (alleyoop_finalize): Free the symtab.

	* src/vgrule-list.c (vg_rule_list_add_rule): Unref the gconf
	client when we finish with it.

	* src/vgmemcheckprefs.c (toggle_button_toggled): Unref the gconf
	client after we're done using it.
	(menu_item_activated): Same.
	(spin_focus_out): And here.
	(vg_memcheck_prefs_init): Same.
	(memcheck_prefs_get_argv): And finally here.

	* src/vghelgrindprefs.c (toggle_button_toggled): Unref the gconf
	client when we finish with it.
	(menu_item_activated): Same.
	(vg_helgrind_prefs_init): And here.
	(helgrind_prefs_get_argv): Here too.

	* src/vggeneralprefs.c (toggle_button_toggled): Unref the gconf
	client when we finish with it.
	(spin_focus_out): Here too.
	(file_entry_changed): And here.
	(vg_general_prefs_init): Same.
	(general_prefs_get_argv): Again here.

	* src/vgcachegrindprefs.c (toggle_button_toggled): Unref the gconf
	client when we finish with it.
	(entry_focus_out): Same.
	(vg_cachegrind_prefs_init): Here too.
	(cachegrind_prefs_get_argv): And finally here.

	* src/prefs.c (entry_changed): Unref the gconf client when we
	finish with it.
	(spin_changed): Same.
	(alleyoop_prefs_init): And here too.

	* src/vgdefaultview.c (vg_default_view_init): Save the gconf
	client on the view.
	(vg_default_view_destroy): Unref the gconf client.
	(vg_default_view_new): Use the view->gconf instead of using
	gconf_client_get_default ().
	(custom_editor_cb): Same.

2003-05-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgerror.c: Disable debug messages.

	* src/process.c: Disable debug messages.

	* src/symtab.c (slurp_symtab): Change warnings to use g_warning
	instead of fprintf.

	* src/alleyoop.c (alleyoop_run): Updated for new process_fork()
	API - we want to redirect stdin/out/err to our stdin/out/err.

	* src/process.c (process_fork): Take a new argument, redirect,
	which decides whether or not we want the child process to redirect
	its stdin/stdout/stderr to our stdin/stdout/stderr.

	* src/symtab.c (symtab_new): Updated for new process_fork() API -
	pass FALSE for redirect here as we don't want the output of ld
	redirected to our stdin/out/err.

2003-05-16  Jordi Mallach  <jordi@sindominio.net>

	* configure.in (ALL_LINGUAS): Add "ca" (Catalan).

2003-05-09  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgrule-list.c: Use a linked list rather than a GPtrArray for
	storing the rules. This way we can quickly remove the rules when
	the user removes them via the UI. Also fixes a bug that relied on
	using the array indexes (which would become wrong if any rules
	with a lower index got removed).

	* src/list.[c,h]: New source files implementing a linked list.

	* src/vgerror.c (vg_error_parser_step): Ignore "IGNORED call to:"
	errors.

2003-05-08  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgrule-list.c (add_response_cb): Don't allow the user to
	save a rule without a name.
	(edit_response_cb): Same.

	* src/vgrule-editor.c (vg_rule_editor_new_from_summary): Set a
	default rule name - saves a lot of typing :-)
	(vg_rule_editor_get_name): New function to get the name of a rule
	from the editor.

	* src/vgerror.c (vg_error_parser_step): Handle Valgrind 1.9.6's
	thread-id output.

2003-05-07  Danilo Šegan  <dsegan@gmx.net>

	* configure.in: Added "sr" and "sr@Latn" to ALL_LINGUAS.

2003-04-29  Jeffrey Stedfast  <fejj@ximian.com>

	* alleyoop.c: s/AddrCheck/Addrcheck/ s/MemCheck/Memcheck/

	Make sure we can write out both Valgrind 1.0.4 and 1.9.5 formatted
	supp files. (if you want to save a 1.0.4 formatted supp rule,
	simply don't check the Addrcheck nor Memcheck checkboxes).

	* src/vgrule-editor.c (vg_rule_editor_get_rule): If the
	Addrcheck/Memcheck checkboxes are checked, add them to the rule.
	(vg_rule_editor_new_from_rule): Set the 'active' state of the
	Addrcheck/Memcheck toggle buttons.
	(vg_rule_editor_init): Create the addrcheck and memcheck
	checkboxes.

2003-04-29  Kjartan Maraas  <kmaraas@gnome.org>

	* configure.in (ALL_LINGUAS): Added "no" (Norwegian)
	
2003-04-26  Evandro Fernandes Giovanini <evandrofg@ig.com.br>

	* configure.in (ALL_LINGUAS): Added "pt_BR" (Brazilian 
	Portuguese).

2003-04-26  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped version to 0.7.1

	* README: Added some basic usage documentation.

	* src/prefs.c (alleyoop_prefs_init): Changed the default editor
	string.

	* src/vgdefaultview.c (custom_editor_cb): Launch the user's custom
	editor command-line.
	(tree_button_press): Add a double-click support - this launches
	the user's configured editor.

2003-04-25  Miloslav Trmac  <mitr@volny.cz>

	* configure.in: Added "cs" (Czech) to ALL_LINGUAS.

2003-04-25  Jeffrey Stedfast  <fejj@ximian.com>

	* src/prefs.c: Add another page.
	(alleyoop_prefs_class_init): Initialise page_types[PAGE_HELGRIND].

	* src/vghelgrindprefs.[c,h]: New VgSkinPrefs subclass which
	implements the config options meant to be used with the Helgrind
	skin.

2003-04-23  Duarte Loreto <happyguy_pt@hotmail.com>

	* configure.in: Added "pt" (Portuguese) to ALL_LINGUAS.

2003-04-23  Christian Rose  <menthos@menthos.com>

	* configure.in: Added "sv" to ALL_LINGUAS.

2003-04-22  Jeffrey Stedfast  <fejj@ximian.com>

	* src/alleyoop.c (run_prompt_argv): New function to prompt for an
	executable (and command-line arguments) to debug.
	(file_run_cb): If alleyoop was started without an argv specified
	in the command-line, prompt for a program to execute.

2003-04-22  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped version to 0.7.0

	* src/vgmemcheckprefs.c: Fix a compiler warning introduced when
	the table fields changed.

2003-04-22  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgrule-list.c (vg_rule_list_new): Connect to the "map"
	signal to load the suppressiosn rather than the "show"
	signal. This fixes the "suppressions file doesn't load" bug that
	no one reported :-)

	* src/vgdefaultview.c (vg_default_view_init): Set the initial
	suppressions filename on the vgrule-list widget.

2003-04-22  Jeffrey Stedfast  <fejj@ximian.com>

	* src/alleyoop.c: Comment out the cachegrind and helgrind menu
	items since we don't support either yet.

	* src/main.c (main): Disable the ability to specify helgrind or
	cachegrind on the command-line (spit out a warning) since we don't
	yet support them.

2003-04-22  Jeffrey Stedfast  <fejj@ximian.com>

	Updated the suppressions rule parser to handle the 1.9.x file
	format as well as the 1.0.x format.

	* src/vgrule.c (vg_rule_new): Initialise the list of skins to
	NULL.
	(vg_rule_add_skin): New function to append a skin to the rule.
	(vg_rule_free): Free the list of skins the rule is meant for.
	(vg_rule_parser_step): If the line containing the suppression type
	also contains a list of skins for which it is meant to be used,
	parse the skin list as well.
	(vg_suppressions_file_append_rule): If the rule is meant to be
	used by only specific skins, make sure to write out the skins it
	is meant to be used with.
	(vg_suppressions_file_write_header): Updated to match the
	suppression header in the Valgrind 1.9.5 distribution.

2003-04-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/prefs.c (alleyoop_prefs_create_argv): Keep track of which
	page we need to grab the specific prefs from.

	* src/main.c: #define POPT_TABLEEND ourselves if it isn't already
	defined by popt.h. This is a workaround for systems such as SuSE
	8.1 which ship with an old version of popt.

2003-04-21  Jeffrey Stedfast  <fejj@ximian.com>

	* src/alleyoop.schemas: Added Cachegrind options.

	* src/prefs.c: Added vgcachegrindprefs support.

	* src/vgcachegrindprefs.[c,h]: New VgSkinPrefs subclass which
	implements the config options meant to be used with the Cachegrind
	skin.

2003-04-20  Jeffrey Stedfast  <fejj@ximian.com>

	* src/main.c (main): Parse a --skin argument (syntax is exactly
	the same as for valgrind). Currently supported are just memcheck
	and addrcheck, however.

	* src/alleyoop.c (alleyoop_new): Now takes a skin argument which
	can be specified on the command-line. Also added a menu to allow
	the user to specify which skin to use for valgrind.
	(alleyoop_run): Pass the current 'skin' preference to
	alleyoop_prefs_create_argv().

	* src/vgmemcheckprefs.c (memcheck_prefs_get_argv): Updated to
	handle both memcheck and addrcheck prefs (since they are virtually
	identical except for one command-line argument).

	* src/vgskinprefs.c (vg_skin_prefs_get_argv): Now takes a skin
	argument.

	* src/prefs.c (alleyoop_prefs_create_argv): Remove the code to add
	the --alignment=8 switch from here. Also now takes a 'skin'
	argument. If the skin is non-NULL, add the --skin= argument.

	* src/vggeneralprefs.c (general_prefs_get_argv): Move the
	alignment switch into here instead. Also updated to take a 'skin'
	argument.

2003-04-19  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped version to 0.6.5

	* src/vgdefaultview.c (resolve_full_path): Make sure the stack
	info's filename is non-NULL before dereferencing it. Also make
	sure the resolved sym->filename is not NULL.
	(valgrind_view_cut): Implemented.
	(valgrind_view_copy): Implemented.
	(valgrind_view_paste): Nothing to do here.

2003-04-19  Jeffrey Stedfast  <fejj@ximian.com>

	* NEWS: Updated.

	* configure.in: Bumped version to 0.6.4

	* src/vgdefaultview.c (vg_default_view_finalize): Free the
	pre-compiled regex if a search is in effect.
	(set_search): New convenience function to set the view search_id
	and search_regex and free the old search_regex if needed. Also
	handles popping up a message dialog if the regex is invalid.
	(search_bar_search): Call set_search().
	(search_bar_clear): Same.
	(view_rebuild): No longer takes an expr argument. Instead, we just
	use view->search_regex.
	(recv_error_cb): Since we now keep the search_regex on the view
	object, we can now apply the current search to any new errors that
	get reported, thus allowing us to update the view as new errors
	come in when a search is active.
	(tree_row_expanded): Don't free the srcbuf until *after* we've set
	it on the treeview model. D'oh!

2003-04-19  Jeffrey Stedfast  <fejj@ximian.com>

	* src/alleyoop.c (alleyoop_new): Don't create a searchbar, this
	needs to be handled by the view.

	* src/vgdefaultview.c (vg_default_view_init): Create our own
	search bar.
	(search_bar_clear): New callback for when the search gets
	cleared. Rebuild the view - show all errors.
	(search_bar_search): New callback for when a search is
	activated. Rebuild the view showing only those errors which match.
	(view_show_error): New convenience function which adds an error to
	the view.
	(error_matches_search): New function to see if an error matches
	the search expression.
	(view_rebuild): New convenience function to rebuild the view using
	the search expression (if there is any).
	(recv_error_cb): If the error matches the search, add it to the
	view using view_show_error() instead of doing it ourselves.

	* src/vgsearchbar.[c,h]: New source files implementing a
	basic search-bar widget.

2003-04-18  Jeffrey Stedfast  <fejj@ximian.com>

	* src/vgdefaultview.c (vg_default_view_new): Get the num-lines
	value from gconf instead of hard-coding it to 2.
	(tree_row_expanded): GtkTreeStore copies strings, so there's no
	need for us to keep a collection of malloc'd strings ourselves.
	(recv_error_cb): Same here.
	(vg_default_view_init): No need to keep an array of malloc'd
	strings anymore, so get rid of view->gc.
	(vg_default_view_finalize): Same here.
	(valgrind_view_clear): And here.

	* src/prefs.c (alleyoop_prefs_init): Added a new preference option
	allowing the user to specify the number of lines above and below
	the target src line he/she wishes to view in a preview.

2003-04-17  Jeffrey Stedfast  <fejj@ximian.com>

	* src/main.c (main): When printing the version, clean up any
	malloc'd memory before exiting.

2003-04-16  Jeffrey Stedfast  <fejj@ximian.com>

	* configure.in: Bumped version to 0.6.3

	* po/POTFILES.in: Updated.

	* src/vgerror.c (vg_error_parser_flush): Only pop the unflushed
	error if we are in a state in which something may have been parsed
	(ie, not NEW_ERROR state). Otherwise free the error and reset the
	state.

	* src/vgdefaultview.c (valgrind_view_clear): After freeing all the
	VgErrors, set the size of the error array to 0 (not the gc size to
	0 again). Copy-paste-o :-)
	(valgrind_view_disconnect): Flush the parser before freeing it to
	flush that last error off to our UI.

	* src/alleyoop.c (edit_clear_cb): New callback to clear the view's
	display.
	(edit_menu): Added a 'clear' menu item.

	* src/main.c (main): Add support for -R, --recursive. This new
	command-line option is similar to -I, --include, except that we
	recursively scan subdirectories and add them to the include path
	automagically.


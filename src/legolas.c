/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "vgio.h"

#include "legolas.h"


struct _ElfDesc {
	char *filename;
	int fd;
	
	union {
		Elf32_Ehdr hdr32;
		Elf64_Ehdr hdr64;
	} hdr;
	
	void *secthdrs;
	
	ElfSection *sections;
	ElfSection *shstrtab;
};

static char *elf_class[ELFCLASSNUM] = {
	"Invalid class",
	"32-bit objects",
	"64-bit objects"
};

static char *elf_data[ELFDATANUM] = {
	"Invalid data encoding",
	"2's compliment, little endian",
	"2's compliment, big endian"
};

#define ELFOSABINUM 256
static char *elf_osabi[ELFOSABINUM] = {
	"UNIX System V",
	"HP-UX",
	"NetbSD",
	"Linux",
	NULL, NULL,
	"Sun Solaris",
	"IBM AIX",
	"SGI Irix",
	"FreeBSD",
	"Compaq TRU64",
	"Novell Modesto",
	"OpenBSD",
};

static char *elf_objtype[ET_NUM] = {
	"No file type",
	"Relocatable file",
	"Executable file",
	"Shared object file",
	"Core file",
};

static char *elf_sym_types[16] = {
	"Unspecified",
	"Object",
	"Function",
	"Section",
	"File",
	"Common",
	"TLS (thread-local)",
};

static const char *
elf_arch (unsigned int arch)
{
	switch (arch) {
	default:
	case EM_NONE: return "No machine";
	case EM_M32: return "AT&T WE 32100";
	case EM_SPARC: return "SUN SPARC";
	case EM_386: return "Intel 80386";
	case EM_68K: return "Motorola m68k family";
	case EM_88K: return "Motorola m88k family";
	case EM_860: return "Intel 80860";
	case EM_MIPS: return "MIPS R3000 big-endian";
	case EM_S370: return "IBM System/370";
	case EM_MIPS_RS3_LE: return "MIPS R3000 little-endian";
		
	case EM_PARISC: return "HPPA";
	case EM_VPP500: return "Fujitsu VPP500";
	case EM_SPARC32PLUS: return "Sun's \"v8plus\"";
	case EM_960: return "Intel 80960";
	case EM_PPC: return "PowerPC";
	case EM_PPC64: return "PowerPC 64-bit";
	case EM_S390: return "IBM S390";
		
	case EM_V800: return "NEC V800 series";
	case EM_FR20: return "Fujitsu FR20";
	case EM_RH32: return "TRW RH-32";
	case EM_RCE: return "Motorola RCE";
	case EM_ARM: return "ARM";
	case EM_FAKE_ALPHA: return "Digital Alpha";
	case EM_SH: return "Hitachi SH";
	case EM_SPARCV9: return "SPARC v9 64-bit";
	case EM_TRICORE: return "Siemens Tricore";
	case EM_ARC: return "Argonaut RISC Core";
	case EM_H8_300: return "Hitachi H8/300";
	case EM_H8_300H: return "Hitachi H8/300H";
	case EM_H8S: return "Hitachi H8S";
	case EM_H8_500: return "Hitachi H8/500";
	case EM_IA_64: return "Intel Merced";
	case EM_MIPS_X: return "Stanford MIPS-X";
	case EM_COLDFIRE: return "Motorola Coldfire";
	case EM_68HC12: return "Motorola M68HC12";
	case EM_MMA: return "Fujitsu Multimedia Accelerator";
	case EM_PCP: return "Siemens PCP";
	case EM_NCPU: return "Sony nCPU embedded RISC";
	case EM_NDR1: return "Denso NDR1 microprocessor";
	case EM_STARCORE: return "Motorola Start*Core processor";
	case EM_ME16: return "Toyota ME16 processor";
	case EM_ST100: return "STMicroelectronic ST100 processor";
	case EM_TINYJ: return "Advanced Logic Corp. Tinyj emb.fam";
	case EM_X86_64: return "AMD x86-64 architecture";
	case EM_PDSP: return "Sony DSP Processor";
		
	case EM_FX66: return "Siemens FX66 microcontroller";
	case EM_ST9PLUS: return "STMicroelectronics ST9+ 8/16 mc";
	case EM_ST7: return "STmicroelectronics ST7 8 bit mc";
	case EM_68HC16: return "Motorola M68HC16 microcontroller";
	case EM_68HC11: return "Motorola M68HC11 microcontroller";
	case EM_68HC08: return "Motorola M68HC08 microcontroller";
	case EM_68HC05: return "Motorola M68HC05 microcontroller";
	case EM_SVX: return "Silicon Graphics SVx";
	case EM_ST19: return "STMicroelectronics ST19 8 bit mc";
	case EM_VAX: return "Digital VAX";
	case EM_CRIS: return "Axis Communications 32-bit embedded processor";
	case EM_JAVELIN: return "Infineon Technologies 32-bit embedded processor";
	case EM_FIREPATH: return "Element 14 64-bit DSP Processor";
	case EM_ZSP: return "LSI Logic 16-bit DSP Processor";
	case EM_MMIX: return "Donald Knuth's educational 64-bit processor";
	case EM_HUANY: return "Harvard University machine-independent object files";
	case EM_PRISM: return "SiTera Prism";
	case EM_AVR: return "Atmel AVR 8-bit microcontroller";
	case EM_FR30: return "Fujitsu FR30";
	case EM_D10V: return "Mitsubishi D10V";
	case EM_D30V: return "Mitsubishi D30V";
	case EM_V850: return "NEC v850";
	case EM_M32R: return "Mitsubishi M32R";
	case EM_MN10300: return "Matsushita MN10300";
	case EM_MN10200: return "Matsushita MN10200";
	case EM_PJ: return "picoJava";
	case EM_OPENRISC: return "OpenRISC 32-bit embedded processor";
	case EM_ARC_A5: return "ARC Cores Tangent-A5";
	case EM_XTENSA: return "Tensilica Xtensa Architecture";
	}
}


static int
elf32_load (ElfDesc *elfd, unsigned char *ident, ElfType expect)
{
	Elf32_Ehdr *hdr32 = &elfd->hdr.hdr32;
	ElfSection *sect, *tail;
	unsigned char *shstrtab;
	unsigned char *sectbuf;
	Elf32_Shdr *secthdrs;
	size_t sectoff, n;
	int fd = elfd->fd;
	int i;
	
	if (hdr32->e_type == ET_NONE || hdr32->e_type >= ET_NUM) {
		errno = ELF_EINVALTYPE;
		return -1;
	}
	
	if (expect && hdr32->e_type != expect) {
		errno = ELF_ENOTTYPE;
		return -1;
	}
	
	if (hdr32->e_machine == EM_NONE) {
		errno = ELF_ENOARCH;
		return -1;
	}
	
	if (hdr32->e_version != EV_CURRENT) {
		errno = ELF_EINVALVER;
		return -1;
	}
	
	if (hdr32->e_shoff != 0) {
		if (hdr32->e_shentsize != sizeof (Elf32_Shdr)) {
			errno = ELF_ESHDRSIZE;
			return -1;
		}
		
		if (lseek (fd, hdr32->e_shoff, SEEK_SET) == -1)
			return -1;
		
		n = hdr32->e_shnum * hdr32->e_shentsize;
		if (!(secthdrs = elfd->secthdrs = malloc (n))) {
			errno = ENOMEM;
			return -1;
		}
		
		/* read in our section headers */
		if (vg_read (fd, (char *) secthdrs, n) == -1)
			return -1;
		
		/* load our shstrtab first */
		if ((i = hdr32->e_shstrndx) != SHN_UNDEF) {
			if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
				return -1;
			
			if (!(shstrtab = malloc (secthdrs[i].sh_size))) {
				errno = ENOMEM;
				return -1;
			}
			
			if (vg_read (fd, shstrtab, secthdrs[i].sh_size) == -1)
				return -1;
		} else {
			shstrtab = NULL;
		}
		
		/* create our linked list of ElfSections */
		if (!(sectbuf = malloc (sizeof (ElfSection) * hdr32->e_shnum))) {
			errno = ENOMEM;
			return -1;
		}
		
		sectoff = 0;
		elfd->sections = NULL;
		tail = (ElfSection *) &elfd->sections;
		
		for (i = 0; i < hdr32->e_shnum; i++, sectoff += sizeof (ElfSection)) {
			sect = (ElfSection *) (sectbuf + sectoff);
			sect->next = NULL;
			sect->name = shstrtab ? shstrtab + secthdrs[i].sh_name : NULL;
			sect->elfd = elfd;
			sect->index = i;
			sect->vma = secthdrs[i].sh_addr;
			sect->data = NULL;
			
			if (secthdrs[i].sh_link)
				sect->link = (ElfSection *) (sectbuf + (secthdrs[i].sh_link * sizeof (ElfSection)));
			else
				sect->link = NULL;
			
			switch (secthdrs[i].sh_type) {
			case SHT_STRTAB:
				if (i != hdr32->e_shstrndx) {
					/* load the string table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else {
					/* this is the shstrtab, we've already loaded it */
					sect->data = shstrtab;
					elfd->shstrtab = sect;
				}
				
				break;
			case SHT_SYMTAB:
			case SHT_DYNSYM:
				/* load the symbol table */
				if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
					return -1;
				
				n = secthdrs[i].sh_entsize * sizeof (Elf32_Sym);
				if (!(sect->data = malloc (n))) {
					errno = ENOMEM;
					return -1;
				}
				
				if (vg_read (fd, sect->data, n) == -1)
					return -1;
				
				break;
			case SHT_DYNAMIC:
				/* load the dynamic linking information */
				if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
					return -1;
				
				n = secthdrs[i].sh_entsize * sizeof (Elf32_Dyn);
				if (!(sect->data = malloc (n))) {
					errno = ENOMEM;
					return -1;
				}
				
				if (vg_read (fd, sect->data, n) == -1)
					return -1;
				
				break;
			case SHT_PROGBITS:
				if (sect->name == NULL)
					break;
				
				if (!strcmp (sect->name, ".interp")) {
					/* contains the path of the interpreter */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					((char *) sect->data)[secthdrs[i].sh_size] = '\0';
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_str")) {
					/* contains the Dwarf2 debug string table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_line")) {
					/* contains the Dwarf2 debug line-info table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_pubnames")) {
					/* contains the Dwarf2 debug pubnames (public symbol strings) */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				}
				break;
			default:
				break;
			}
			
			tail->next = sect;
			tail = sect;
		}
	}
	
	return 0;
}


static int
elf64_load (ElfDesc *elfd, unsigned char *ident, ElfType expect)
{
	Elf64_Ehdr *hdr64 = &elfd->hdr.hdr64;
	ElfSection *sect, *tail;
	unsigned char *shstrtab;
	unsigned char *sectbuf;
	Elf64_Shdr *secthdrs;
	size_t sectoff, n;
	int fd = elfd->fd;
	int i;
	
	if (hdr64->e_type == ET_NONE || hdr64->e_type >= ET_NUM) {
		errno = ELF_EINVALTYPE;
		return -1;
	}
	
	if (expect && hdr64->e_type != expect) {
		errno = ELF_ENOTTYPE;
		return -1;
	}
	
	if (hdr64->e_machine == EM_NONE) {
		errno = ELF_ENOARCH;
		return -1;
	}
	
	if (hdr64->e_version != EV_CURRENT) {
		errno = ELF_EINVALVER;
		return -1;
	}
	
	if (hdr64->e_shoff != 0) {
		if (hdr64->e_shentsize != sizeof (Elf64_Shdr)) {
			errno = ELF_ESHDRSIZE;
			return -1;
		}
		
		if (lseek (fd, hdr64->e_shoff, SEEK_SET) == -1)
			return -1;
		
		n = hdr64->e_shnum * hdr64->e_shentsize;
		if (!(secthdrs = elfd->secthdrs = malloc (n))) {
			errno = ENOMEM;
			return -1;
		}
		
		/* read in our section headers */
		if (vg_read (fd, (char *) secthdrs, n) == -1)
			return -1;
		
		/* load our shstrtab first */
		if ((i = hdr64->e_shstrndx) != SHN_UNDEF) {
			if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
				return -1;
			
			if (!(shstrtab = malloc (secthdrs[i].sh_size))) {
				errno = ENOMEM;
				return -1;
			}
			
			if (vg_read (fd, shstrtab, secthdrs[i].sh_size) == -1)
				return -1;
		} else {
			shstrtab = NULL;
		}
		
		/* create our linked list of ElfSections */
		if (!(sectbuf = malloc (sizeof (ElfSection) * hdr64->e_shnum))) {
			errno = ENOMEM;
			return -1;
		}
		
		sectoff = 0;
		elfd->sections = NULL;
		tail = (ElfSection *) &elfd->sections;
		
		for (i = 0; i < hdr64->e_shnum; i++, sectoff += sizeof (ElfSection)) {
			sect = (ElfSection *) (sectbuf + sectoff);
			sect->next = NULL;
			sect->name = shstrtab ? shstrtab + secthdrs[i].sh_name : NULL;
			sect->elfd = elfd;
			sect->index = i;
			sect->vma = secthdrs[i].sh_addr;
			sect->data = NULL;
			
			if (secthdrs[i].sh_link)
				sect->link = (ElfSection *) (sectbuf + (secthdrs[i].sh_link * sizeof (ElfSection)));
			else
				sect->link = NULL;
			
			switch (secthdrs[i].sh_type) {
			case SHT_STRTAB:
				if (i != hdr64->e_shstrndx) {
					/* load the string table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else {
					/* this is the shstrtab, we've already loaded it */
					sect->data = shstrtab;
					elfd->shstrtab = sect;
				}
				
				break;
			case SHT_SYMTAB:
			case SHT_DYNSYM:
				/* load the symbol table */
				if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
					return -1;
				
				n = secthdrs[i].sh_entsize * sizeof (Elf32_Sym);
				if (!(sect->data = malloc (n))) {
					errno = ENOMEM;
					return -1;
				}
				
				if (vg_read (fd, sect->data, n) == -1)
					return -1;
				
				break;
			case SHT_DYNAMIC:
				/* load the dynamic linking information */
				if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
					return -1;
				
				n = secthdrs[i].sh_entsize * sizeof (Elf32_Dyn);
				if (!(sect->data = malloc (n))) {
					errno = ENOMEM;
					return -1;
				}
				
				if (vg_read (fd, sect->data, n) == -1)
					return -1;
				
				break;
			case SHT_PROGBITS:
				if (sect->name == NULL)
					break;
				
				if (!strcmp (sect->name, ".interp")) {
					/* contains the path of the interpreter */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					((char *) sect->data)[secthdrs[i].sh_size] = '\0';
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_str")) {
					/* contains the Dwarf2 debug string table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_line")) {
					/* contains the Dwarf2 debug line-info table */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				} else if (!strcmp (sect->name, ".debug_pubnames")) {
					/* contains the Dwarf2 debug pubnames (public symbol strings) */
					if (lseek (fd, secthdrs[i].sh_offset, SEEK_SET) == -1)
						return -1;
					
					if (!(sect->data = malloc (secthdrs[i].sh_size + 1))) {
						errno = ENOMEM;
						return -1;
					}
					
					if (vg_read (fd, sect->data, secthdrs[i].sh_size) == -1)
						return -1;
				}
				break;
			default:
				break;
			}
			
			tail->next = sect;
			tail = sect;
		}
	}
	
	return 0;
}


ElfDesc *
elf_open (const char *filename, ElfType expect)
{
	unsigned char ident[EI_NIDENT], *hdr;
	ElfDesc *elfd = NULL;
	ElfSection *sect;
	int errnosav, fd;
	size_t n;
	
	if ((fd = open (filename, O_RDONLY)) == -1)
		return NULL;
	
	if (vg_read (fd, ident, EI_NIDENT) == -1) {
		errnosav = errno;
		while (close (fd) == -1)
			;
		errno = errnosav;
		
		return NULL;
	}
	
	if (memcmp (ident, ELFMAG, SELFMAG) != 0) {
		while (close (fd) == -1)
			;
		
		errno = ELF_ENOTELF;
		
		return NULL;
	}
	
	if (ident[EI_CLASS] == ELFCLASSNONE || ident[EI_CLASS] >= ELFCLASSNUM) {
		while (close (fd) == -1)
			;
		
		errno = ELF_EINVALCLASS;
		
		return NULL;
	}
	
	if (ident[EI_DATA] == ELFDATANONE || ident[EI_DATA] >= ELFDATANUM) {
		while (close (fd) == -1)
			;
		
		errno = ELF_EINVALENC;
		
		return NULL;
	}
	
	if (ident[EI_VERSION] != EV_CURRENT) {
		while (close (fd) == -1)
			;
		
		errno = ELF_EINVALVER;
		
		return NULL;
	}
	
	if (elf_osabi [ident[EI_OSABI]] == NULL) {
		while (close (fd) == -1)
			;
		
		errno = ELF_EINVALOSABI;
		
		return NULL;
	}
	
	if (!(elfd = calloc (1, sizeof (ElfDesc)))) {
		while (close (fd) == -1)
			;
		
		errno = ENOMEM;
		
		return NULL;
	}
	
	if (!(elfd->filename = strdup (filename))) {
		while (close (fd) == -1)
			;
		
		free (elfd);
		
		errno = ENOMEM;
		
		return NULL;
	}
	
	elfd->fd = fd;
	
	if (ident[EI_CLASS] == ELFCLASS32) {
		n = sizeof (Elf32_Ehdr) - EI_NIDENT;
	} else if (ident[EI_CLASS] == ELFCLASS64) {
		n = sizeof (Elf64_Ehdr) - EI_NIDENT;
	} else {
		errno = ELF_EINVALCLASS;
		goto exception;
	}
	
	hdr = (unsigned char *) &elfd->hdr;
	memcpy (hdr, ident, EI_NIDENT);
	hdr += EI_NIDENT;
	
	if (vg_read (fd, hdr, n) == -1) {
		errnosav = errno;
		
		while (close (fd) == -1)
			;
		
		free (elfd->filename);
		free (elfd);
		
		errno = errnosav;
		
		return NULL;
	}
	
	if (ident[EI_CLASS] == ELFCLASS32) {
		if (elf32_load (elfd, ident, expect) == -1)
			goto exception;
	} else {
		if (elf64_load (elfd, ident, expect) == -1)
			goto exception;
	}
	
	return elfd;
	
 exception:
	
	errnosav = errno;
	
	while (close (fd) == -1)
		;
	
	errno = errnosav;
	
	sect = elfd->sections;
	while (sect != NULL) {
		free (sect->data);
		sect = sect->next;
	}
	
	free (elfd->filename);
	free (elfd->secthdrs);
	free (elfd->sections);  /* yes, this free's the entire linked list. isn't my code leet? */
	free (elfd);
	
	return NULL;
}


void
elf_close (ElfDesc *elfd)
{
	ElfSection *sect;
	
	while (close (elfd->fd) == -1)
		;
	
	sect = elfd->sections;
	while (sect != NULL) {
		free (sect->data);
		sect = sect->next;
	}
	
	free (elfd->filename);
	free (elfd->secthdrs);
	free (elfd->sections);  /* yes, this free's the entire linked list. isn't my code leet? */
	free (elfd);
}


const char *
elf_filename (ElfDesc *elfd)
{
	return elfd->filename;
}


unsigned int
elf_flags (ElfDesc *elfd)
{
	/* FIXME: implement me */
	return 0;
}


static void
elf32_sym_dump (Elf32_Sym *sym, const char *strtab)
{
	unsigned char st_type;
	const char *type;
	
	st_type = ELF32_ST_TYPE (sym->st_info);
	type = elf_sym_types[st_type];
	
	printf ("    Symbol name offset              %u\n", sym->st_name);
	if (strtab != NULL)
		printf ("    Symbol name                     %s\n", strtab + sym->st_name);
	printf ("    Symbol value                    %u\n", sym->st_value);
	printf ("    Symbol size                     %u\n", sym->st_size);
	printf ("    Symbol binding                  %u\n", ELF32_ST_BIND (sym->st_info));
	printf ("    Symbol type                     %u (%s)\n", st_type, type ? type : "OS Specific");
	printf ("    Symbol other                    %u\n", sym->st_other);
	printf ("    Symbol shindex                  %u\n\n", sym->st_shndx);
}

static void
elf64_sym_dump (Elf64_Sym *sym, const char *strtab)
{
	unsigned char st_type;
	const char *type;
	
	st_type = ELF64_ST_TYPE (sym->st_info);
	type = elf_sym_types[st_type];
	
	printf ("    Symbol name offset              %u\n", sym->st_name);
	if (strtab != NULL)
		printf ("    Symbol name                     %s\n", strtab + sym->st_name);
	printf ("    Symbol value                    %u\n", sym->st_value);
	printf ("    Symbol size                     %u\n", sym->st_size);
	printf ("    Symbol binding                  %u\n", ELF64_ST_BIND (sym->st_info));
	printf ("    Symbol type                     %u (%s)\n", st_type, type ? type : "OS Specific");
	printf ("    Symbol other                    %u\n", sym->st_other);
	printf ("    Symbol shindex                  %u\n\n", sym->st_shndx);
}

static void
elf32_dyn_dump (Elf32_Dyn *dyn, const char *strtab)
{
	printf ("    Dynamic linking tag:            %u\n", dyn->d_tag);
	printf ("    Dynamic linking val/addr:       %u\n", dyn->d_un.d_val);
	if (strtab && dyn->d_tag == DT_NEEDED)
		printf ("    Dynamically Linked Library:     %s\n", strtab + dyn->d_un.d_val);
	printf ("\n");
}

static void
elf64_dyn_dump (Elf64_Dyn *dyn, const char *strtab)
{
	printf ("    Dynamic linking tag:            %u\n", dyn->d_tag);
	printf ("    Dynamic linking val/addr:       %u\n", dyn->d_un.d_val);
	if (strtab && dyn->d_tag == DT_NEEDED)
		printf ("    Dynamically Linked Library:     %s\n", strtab + dyn->d_un.d_val);
	printf ("\n");
}

static void
elf32_section_dump (ElfSection *sect)
{
	Elf32_Shdr *secthdrs;
	Elf32_Sym *syms;
	Elf32_Dyn *dyn;
	char *strtab;
	size_t i, n;
	
	secthdrs = (Elf32_Shdr *) sect->elfd->secthdrs;
	i = sect->index;
	
	printf ("Section name index:                 %u\n", secthdrs[i].sh_name);
	if (sect->name != NULL)
		printf ("Section name:                       %s\n", sect->name);
	printf ("Section type:                       %u\n", secthdrs[i].sh_type);
	printf ("Section flags:                      %u\n", secthdrs[i].sh_flags);
	printf ("Section virtual addr at execution:  0x%x\n", secthdrs[i].sh_addr);
	printf ("Section file offset:                %u\n", secthdrs[i].sh_offset);
	printf ("Section size in bytes:              %u\n", secthdrs[i].sh_size);
	printf ("Link to another section:            %u\n", secthdrs[i].sh_link);
	printf ("Additional section information:     %u\n", secthdrs[i].sh_info);
	printf ("Section alignment:                  %u\n", secthdrs[i].sh_addralign);
	printf ("Entry size if section holds table:  %u\n", secthdrs[i].sh_entsize);
	
	if (sect->data != NULL) {
		switch (secthdrs[i].sh_type) {
		case SHT_SYMTAB:
		case SHT_DYNSYM:
			printf ("\n  Symbol Table contains:\n\n");
			syms = sect->data;
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			n = secthdrs[i].sh_entsize;
			for (i = 0; i < n; i++)
				elf32_sym_dump (&syms[i], strtab);
			break;
		case SHT_DYNAMIC:
			printf ("\n  Dynamic Linking Information:\n\n");
			dyn = sect->data;
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			n = secthdrs[i].sh_entsize;
			for (i = 0; i < n; i++)
				elf32_dyn_dump (&dyn[i], strtab);
			break;
		default:
			printf ("\n");
			break;
		}
	} else {
		printf ("\n");
	}
}

static void
elf64_section_dump (ElfSection *sect)
{
	Elf64_Shdr *secthdrs;
	Elf64_Sym *syms;
	Elf64_Dyn *dyn;
	char *strtab;
	size_t i, n;
	
	secthdrs = (Elf64_Shdr *) sect->elfd->secthdrs;
	i = sect->index;
	
	printf ("Section name index:                 %u\n", secthdrs[i].sh_name);
	if (sect->name != NULL)
		printf ("Section name:                       %s\n", sect->name);
	printf ("Section type:                       %u\n", secthdrs[i].sh_type);
	printf ("Section flags:                      %u\n", secthdrs[i].sh_flags);
	printf ("Section virtual addr at execution:  0x%x\n", secthdrs[i].sh_addr);
	printf ("Section file offset:                %u\n", secthdrs[i].sh_offset);
	printf ("Section size in bytes:              %u\n", secthdrs[i].sh_size);
	printf ("Link to another section:            %u\n", secthdrs[i].sh_link);
	printf ("Additional section information:     %u\n", secthdrs[i].sh_info);
	printf ("Section alignment:                  %u\n", secthdrs[i].sh_addralign);
	printf ("Entry size if section holds table:  %u\n", secthdrs[i].sh_entsize);
	
	if (sect->data != NULL) {
		switch (secthdrs[i].sh_type) {
		case SHT_SYMTAB:
		case SHT_DYNSYM:
			printf ("\nSymbol Table contains:\n");
			syms = sect->data;
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			n = secthdrs[i].sh_entsize;
			for (i = 0; i < n; i++)
				elf64_sym_dump (&syms[i], strtab);
			break;
		case SHT_DYNAMIC:
			printf ("\nDynamic Linking Information:\n");
			dyn = sect->data;
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			n = secthdrs[i].sh_entsize;
			for (i = 0; i < n; i++)
				elf64_dyn_dump (&dyn[i], strtab);
			break;
		default:
			printf ("\n");
			break;
		}
	} else {
		printf ("\n");
	}
}

static void
elf_hdr32_dump (Elf32_Ehdr *hdr32)
{
	printf ("Object file type:                   %s\n", elf_objtype[hdr32->e_type]);
	printf ("Architecture:                       %s\n", elf_arch (hdr32->e_machine));
	printf ("Object file version:                %u\n", hdr32->e_version);
	printf ("Entry point virtual address:        0x%x\n", hdr32->e_entry);
	printf ("Program header table file offset:   %u\n", hdr32->e_phoff);
	printf ("Section header table file offset:   %u\n", hdr32->e_shoff);
	printf ("Processor-specific flags:           %u\n", hdr32->e_flags);
	printf ("ELF header size in bytes:           %u\n", hdr32->e_ehsize);
	printf ("Program header table entry size:    %u\n", hdr32->e_phentsize);
	printf ("Program header table entry count:   %u\n", hdr32->e_phnum);
	printf ("Section header table entry size:    %u\n", hdr32->e_shentsize);
	printf ("Section header table entry count:   %u\n", hdr32->e_shnum);
	printf ("Section header string table index:  %u\n", hdr32->e_shstrndx);
}

static void
elf_hdr64_dump (Elf64_Ehdr *hdr64)
{
	printf ("Object file type:                   %s\n", elf_objtype[hdr64->e_type]);
	printf ("Architecture:                       %s\n", elf_arch (hdr64->e_machine));
	printf ("Object file version:                %u\n", hdr64->e_version);
	printf ("Entry point virtual address:        0x%x\n", hdr64->e_entry);
	printf ("Program header table file offset:   %u\n", hdr64->e_phoff);
	printf ("Section header table file offset:   %u\n", hdr64->e_shoff);
	printf ("Processor-specific flags:           %u\n", hdr64->e_flags);
	printf ("ELF header size in bytes:           %u\n", hdr64->e_ehsize);
	printf ("Program header table entry size:    %u\n", hdr64->e_phentsize);
	printf ("Program header table entry count:   %u\n", hdr64->e_phnum);
	printf ("Section header table entry size:    %u\n", hdr64->e_shentsize);
	printf ("Section header table entry count:   %u\n", hdr64->e_shnum);
	printf ("Section header string table index:  %u\n", hdr64->e_shstrndx);
}

void
elf_dump (ElfDesc *elfd)
{
	unsigned char *ident;
	ElfSection *sect;
	
	ident = elfd->hdr.hdr32.e_ident;
	
	printf ("Object sizes:                       %s\n", elf_class[ident[EI_CLASS]]);
	printf ("Data encoding:                      %s\n", elf_data[ident[EI_DATA]]);
	printf ("ELF version:                        %d\n", (int) ident[EI_VERSION]);
	printf ("Operating System ABI:               %s\n", elf_osabi[ident[EI_OSABI]]);
	printf ("OS ABI version:                     %d\n", (int) ident[EI_ABIVERSION]);
	printf ("--- padding ---\n");
	
	if (ident[EI_CLASS] == ELFCLASS32)
		elf_hdr32_dump (&elfd->hdr.hdr32);
	else
		elf_hdr64_dump (&elfd->hdr.hdr64);
	
	printf ("\nELF Sections:\n\n");
	
	sect = elfd->sections;
	while (sect != NULL) {
		printf ("Section %u:\n", sect->index);
		if (ident[EI_CLASS] == ELFCLASS32)
			elf32_section_dump (sect);
		else
			elf64_section_dump (sect);
		sect = sect->next;
	}
}


void
elf_shared_libs_foreach (ElfDesc *elfd, ElfSharedLibsForeachFunc cb, void *user_data)
{
	const char *strtab, *libname;
	unsigned char *ident;
	ElfSection *sect;
	size_t i, n;
	
	ident = elfd->hdr.hdr32.e_ident;
	
	sect = elfd->sections;
	if (ident[EI_CLASS] == ELFCLASS32) {
		Elf32_Shdr *secthdrs = elfd->secthdrs;
		Elf32_Dyn *dyn;
		
		while (sect != NULL) {
			dyn = sect->data;
			if (secthdrs[sect->index].sh_type == SHT_DYNAMIC && dyn)
				strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			else
				strtab = NULL;
			
			if (strtab != NULL) {
				n = secthdrs[sect->index].sh_entsize;
				for (i = 0; i < n; i++) {
					if (dyn[i].d_tag == DT_NEEDED) {
						libname = strtab + dyn[i].d_un.d_val;
						if (*libname)
							cb (elfd, libname, user_data);
					}
				}
			}
			
			sect = sect->next;
		}
	} else {
		Elf64_Shdr *secthdrs = elfd->secthdrs;
		Elf64_Dyn *dyn;
		
		while (sect != NULL) {
			dyn = sect->data;
			if (secthdrs[sect->index].sh_type == SHT_DYNAMIC && dyn)
				strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			else
				strtab = NULL;
			
			if (strtab != NULL) {
				n = secthdrs[sect->index].sh_entsize;
				for (i = 0; i < n; i++) {
					if (dyn[i].d_tag == DT_NEEDED) {
						libname = strtab + dyn[i].d_un.d_val;
						if (*libname)
							cb (elfd, libname, user_data);
					}
				}
			}
			
			sect = sect->next;
		}
	}
}


const ElfSection *
elf_get_section_by_name (ElfDesc *elfd, const char *name)
{
	ElfSection *sect;
	
	if (name == NULL)
		return NULL;
	
	if (elfd->shstrtab == NULL || elfd->shstrtab->data == NULL)
		return NULL;
	
	sect = elfd->sections;
	while (sect != NULL) {
		if (sect->name && !strcmp (sect->name, name))
			return sect;
		
		sect = sect->next;
	}
	
	return NULL;
}


size_t
elf_section_get_size_before_reloc (const ElfSection *section)
{
	unsigned char *ident;
	ElfDesc *elfd;
	size_t i;
	
	elfd = section->elfd;
	i = section->index;
	
	ident = elfd->hdr.hdr32.e_ident;
	
	if (ident[EI_CLASS] == ELFCLASS32) {
		Elf32_Shdr *secthdrs = (Elf32_Shdr *) elfd->secthdrs;
		
		return secthdrs[i].sh_entsize;
	} else {
		Elf64_Shdr *secthdrs = (Elf64_Shdr *) elfd->secthdrs;
		
		return secthdrs[i].sh_entsize;
	}
}


static ElfSymbol *
elf32_get_symbols (ElfDesc *elfd)
{
	ElfSymbol *symbols, *sym, *tail;
	Elf32_Shdr *secthdrs;
	ElfSection *sect;
	Elf32_Sym *syms;
	char *strtab;
	size_t i, n;
	
	symbols = NULL;
	tail = (ElfSymbol *) &symbols;
	
	secthdrs = (Elf32_Shdr *) elfd->secthdrs;
	
	sect = elfd->sections;
	while (sect != NULL) {
		switch (secthdrs[sect->index].sh_type) {
		case SHT_SYMTAB:
		case SHT_DYNSYM:
			/* sect->link points to our string table section */
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			syms = sect->data;
			
			n = secthdrs[sect->index].sh_entsize;
			for (i = 0; i < n; i++) {
				if (!(sym = malloc (sizeof (ElfSymbol)))) {
					errno = ENOMEM;
					goto exception;
				}
				
				sym->next = NULL;
				sym->sect = sect;
				sym->name = strtab ? strtab + syms[i].st_name : NULL;
				sym->elfd = elfd;
				sym->index = i;
				
				tail->next = sym;
				tail = sym;
			}
			break;
		default:
			break;
		}
		
		sect = sect->next;
	}
	
	return symbols;
	
 exception:
	
	sym = symbols;
	while (sym != NULL) {
		tail = sym->next;
		free (sym);
		sym = tail;
	}
	
	return NULL;
}


static ElfSymbol *
elf64_get_symbols (ElfDesc *elfd)
{
	ElfSymbol *symbols, *sym, *tail;
	Elf64_Shdr *secthdrs;
	ElfSection *sect;
	Elf64_Sym *syms;
	char *strtab;
	size_t i, n;
	
	symbols = NULL;
	tail = (ElfSymbol *) &symbols;
	
	secthdrs = (Elf64_Shdr *) elfd->secthdrs;
	
	sect = elfd->sections;
	while (sect != NULL) {
		switch (secthdrs[sect->index].sh_type) {
		case SHT_SYMTAB:
		case SHT_DYNSYM:
			/* sect->link points to our string table section */
			strtab = sect->link && sect->link->data ? sect->link->data : NULL;
			syms = sect->data;
			
			n = secthdrs[sect->index].sh_entsize;
			for (i = 0; i < n; i++) {
				if (!(sym = malloc (sizeof (ElfSymbol)))) {
					errno = ENOMEM;
					goto exception;
				}
				
				sym->next = NULL;
				sym->sect = sect;
				sym->name = strtab ? strtab + syms[i].st_name : NULL;
				sym->elfd = elfd;
				sym->index = i;
				
				tail->next = sym;
				tail = sym;
			}
			break;
		default:
			break;
		}
		
		sect = sect->next;
	}
	
	return symbols;
	
 exception:
	
	sym = symbols;
	while (sym != NULL) {
		tail = sym->next;
		free (sym);
		sym = tail;
	}
	
	return NULL;
}


ElfSymbol *
elf_get_symbols (ElfDesc *elfd)
{
	unsigned char *ident;
	
	ident = elfd->hdr.hdr32.e_ident;
	
	if (ident[EI_CLASS] == ELFCLASS32)
		return elf32_get_symbols (elfd);
	else
		return elf64_get_symbols (elfd);
}


void
elf_symbols_free (ElfSymbol *symbols)
{
	ElfSymbol *sym, *n;
	
	sym = symbols;
	while (sym != NULL) {
		n = sym->next;
		free (sym);
		sym = n;
	}
}


static int
elf32_find_nearest_line (ElfDesc *elfd, const ElfSection *section, ElfSymbol **syms,
			 vma_t vma, const char **filename, const char **symname,
			 unsigned int *lineno)
{
	return 0;
}

static int
elf64_find_nearest_line (ElfDesc *elfd, const ElfSection *section, ElfSymbol **syms,
			 vma_t vma, const char **filename, const char **symname,
			 unsigned int *lineno)
{
	return 0;
}


int
elf_find_nearest_line (ElfDesc *elfd, const ElfSection *section, ElfSymbol **syms,
		       vma_t vma, const char **filename, const char **symname,
		       unsigned int *lineno)
{
	unsigned char *ident;
	
	ident = elfd->hdr.hdr32.e_ident;
	
	if (ident[EI_CLASS] == ELFCLASS32)
		return elf32_find_nearest_line (elfd, section, syms, vma, filename, symname, lineno);
	else
		return elf64_find_nearest_line (elfd, section, syms, vma, filename, symname, lineno);
}


const char *
elf_strerror (int err)
{
	if (err >= 0)
		return strerror (err);
	
	switch (err) {
	case ELF_ENOTELF: return "Not an ELF object";
	case ELF_EINVALCLASS: return "Invalid ELF class";
	case ELF_EINVALENC: return "Invalid ELF encoding";
	case ELF_EINVALVER: return "Invalid ELF version";
	case ELF_EINVALOSABI: return "Invalid ELF OS ABI";
	case ELF_EINVALTYPE: return "Invalid ELF object type";
	case ELF_ENOTTYPE: return "Not expected ELF object type";
	case ELF_ENOARCH: return "ELF object is of unknown machine type";
	case ELF_ESHDRSIZE: return "Unexpected ELF section header size";
	}
	
	return "Not an ELF error";
}

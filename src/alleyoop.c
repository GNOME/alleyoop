/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2011 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gconf/gconf-client.h>

#include "vgdefaultview.h"
#include "menu-utils.h"
#include "alleyoop.h"
#include "process.h"
#include "prefs.h"


static char *tool_names[] = {
	"memcheck",
	"addrcheck",
	"cachegrind",
	"helgrind",
};

static void alleyoop_class_init (AlleyoopClass *klass);
static void alleyoop_init (Alleyoop *alleyoop);
static void alleyoop_destroy (GtkObject *obj);
static void alleyoop_finalize (GObject *obj);

/*static void tree_row_expanded (GtkTreeView *treeview, GtkTreeIter *root, GtkTreePath *path, gpointer user_data);
  static gboolean tree_button_press (GtkWidget *treeview, GdkEventButton *event, gpointer user_data);*/

static gboolean io_ready_cb (GIOChannel *gio, GIOCondition condition, gpointer user_data);


static GtkWindowClass *parent_class = NULL;


GType
alleyoop_get_type (void)
{
	static GType type = 0;
	
	if (!type) {
		static const GTypeInfo info = {
			sizeof (AlleyoopClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) alleyoop_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (Alleyoop),
			0,    /* n_preallocs */
			(GInstanceInitFunc) alleyoop_init,
		};
		
		type = g_type_register_static (GTK_TYPE_WINDOW, "Alleyoop", &info, 0);
	}
	
	return type;
}

static void
alleyoop_class_init (AlleyoopClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_ref (GTK_TYPE_WINDOW);
	
	object_class->finalize = alleyoop_finalize;
	gtk_object_class->destroy = alleyoop_destroy;
}

static void
alleyoop_init (Alleyoop *grind)
{
	grind->argv = NULL;
	grind->srcdir = NULL;
	
	grind->view = NULL;
	grind->ui = NULL;
	
	grind->gio = NULL;
	grind->watch_id = 0;
	grind->pid = (pid_t) -1;
	
	grind->toolbar_run = NULL;
	grind->toolbar_kill = NULL;
	
	grind->about = NULL;
	grind->prefs = NULL;
}

static void
alleyoop_finalize (GObject *obj)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	if (grind->gio)
		g_io_channel_unref (grind->gio);
	
	if (grind->ui)
		g_object_unref (grind->ui);
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
alleyoop_destroy (GtkObject *obj)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	if (grind->prefs) {
		gtk_widget_destroy (grind->prefs);
		grind->prefs = NULL;
	}
	
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}


static char **
run_prompt_argv (Alleyoop *grind)
{
	GtkWidget *vbox, *hbox, *entry;
	GtkWidget *dialog, *widget;
	GError *err = NULL;
	const char *command;
	char **argv = NULL;
	int argc;
	
	dialog = gtk_dialog_new_with_buttons (_("Run Executable..."), (GtkWindow *) grind,
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_EXECUTE, GTK_RESPONSE_OK, NULL);
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width ((GtkContainer *) vbox, 6);
	
	hbox = gtk_hbox_new (FALSE, 6);
	
	widget = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	
	widget = gtk_label_new (_("Enter the path to an executable and\nany arguments you wish to pass to it."));
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) vbox, hbox, FALSE, FALSE, 0);
	
	/* FIXME: use a GtkFileChooserButton instead? */
	entry = gtk_entry_new ();
	gtk_widget_show (entry);
	gtk_box_pack_start ((GtkBox *) vbox, entry, FALSE, FALSE, 0);
	
	gtk_widget_show (vbox);
	gtk_box_pack_start ((GtkBox *) ((GtkDialog *) dialog)->vbox, vbox, FALSE, FALSE, 0);
	
 reprompt:
	if (gtk_dialog_run ((GtkDialog *) dialog) == GTK_RESPONSE_OK) {
		command = gtk_entry_get_text ((GtkEntry *) entry);
		if (!g_shell_parse_argv (command, &argc, &argv, &err)) {
			GtkWidget *msg;
			
			msg = gtk_message_dialog_new ((GtkWindow *) dialog, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", err->message);
			g_error_free (err);
			err = NULL;
			
			gtk_dialog_run ((GtkDialog *) msg);
			gtk_widget_destroy (msg);
			
			argv = NULL;
			goto reprompt;
		}
	}
	
	gtk_widget_destroy (dialog);
	
	return argv;
}

static void
file_run_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	GError *err = NULL;
	GtkWidget *dialog;
	
	if (grind->argv == NULL) {
		char **argv;
		
		if (!(argv = run_prompt_argv (grind)))
			return;
		
		g_object_set_data_full ((GObject *) grind, "argv", argv, (GDestroyNotify) g_strfreev);
		grind->argv = (const char **) argv;
	}
	
	alleyoop_run (grind, &err);
	
	if (err != NULL) {
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", err->message);
		
		gtk_dialog_run ((GtkDialog *) dialog);
		gtk_widget_destroy (dialog);
		
		g_error_free (err);
	}
}

static void
file_kill_cb (GtkWidget *widget, gpointer user_data)
{
	alleyoop_kill (user_data);
}

static void
open_ok_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	GtkWidget *filesel;
	int fd;
	
	filesel = g_object_get_data ((GObject *) grind, "open_filesel");
	filename = gtk_file_selection_get_filename ((GtkFileSelection *) filesel);
	
	if (filename != NULL) {
		if ((fd = open (filename, O_RDONLY)) != -1) {
			vg_tool_view_connect ((VgToolView *) grind->view, fd);
			
			grind->pid = (pid_t) -1;
			grind->gio = g_io_channel_unix_new (fd);
			grind->watch_id = g_io_add_watch (grind->gio, G_IO_IN | G_IO_HUP, io_ready_cb, grind);
			
			gtk_widget_set_sensitive (grind->toolbar_run, FALSE);
			gtk_widget_set_sensitive (grind->toolbar_kill, TRUE);
		} else {
			GtkWidget *dialog;
			
			dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
							 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
							 _("Could not load `%s': %s"), filename,
							 g_strerror (errno));
			
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			
			gtk_widget_show (dialog);
		}
	}
	
	gtk_widget_destroy (filesel);
}

static void
open_notify_cb (Alleyoop *grind, GObject *deadbeef)
{
	g_object_set_data ((GObject *) grind, "open_filesel", NULL);
}

static void
file_open_cb (GtkWidget *widget, gpointer user_data)
{
	GtkFileSelection *filesel;
	
	if ((filesel = g_object_get_data ((GObject *) user_data, "open_filesel"))) {
		gdk_window_raise (((GtkWidget *) filesel)->window);
		return;
	}
	
	filesel = (GtkFileSelection *) gtk_file_selection_new (_("Load Valgrind log..."));
	g_signal_connect (filesel->ok_button, "clicked", G_CALLBACK (open_ok_cb), user_data);
	g_object_set_data ((GObject *) user_data, "open_filesel", filesel);
	
	g_signal_connect_swapped (filesel->cancel_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 
	
	g_object_weak_ref ((GObject *) filesel, (GWeakNotify) open_notify_cb, user_data);
	
	gtk_widget_show ((GtkWidget *) filesel);
}

static void
save_valgrind_log (Alleyoop *grind, const char *filename, int flags)
{
	GtkWidget *dialog;
	int response;
	int fd;
	
	flags |= O_WRONLY | O_CREAT | O_TRUNC;
	
 retry:
	if ((fd = open (filename, flags, 0666)) != -1) {
		int errnosav;
		
		if (vg_tool_view_save_log ((VgToolView *) grind->view, fd) == -1) {
			errnosav = errno;
			close (fd);
			unlink (filename);
			errno = errnosav;
			goto exception;
		}
		
		close (fd);
	} else if (flags & O_EXCL && errno == EEXIST) {
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
						 _("Could not save to `%s': %s\nOverwrite?"), filename,
						 g_strerror (errno));
		
		response = gtk_dialog_run ((GtkDialog *) dialog);
		gtk_widget_destroy (dialog);
		
		if (response == GTK_RESPONSE_YES) {
			flags &= ~O_EXCL;
			goto retry;
		}
	} else {
	exception:
		
		dialog = gtk_message_dialog_new ((GtkWindow *) grind, GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
						 _("Could not save to `%s': %s"), filename,
						 g_strerror (errno));
		
		g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
		
		gtk_widget_show (dialog);
	}
}

static void
save_ok_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	GtkWidget *filesel;
	
	filesel = g_object_get_data ((GObject *) grind, "save_filesel");
	filename = gtk_file_selection_get_filename ((GtkFileSelection *) filesel);
	
	if (filename != NULL) {
		g_free (g_object_get_data ((GObject *) grind, "save_filename"));
		g_object_set_data_full ((GObject *) grind, "save_filename", g_strdup (filename), (GDestroyNotify) g_free);
	}
	
	save_valgrind_log (grind, filename, O_EXCL);
	
	gtk_widget_destroy (filesel);
}

static void
save_notify_cb (Alleyoop *grind, GObject *deadbeef)
{
	g_object_set_data ((GObject *) grind, "save_filesel", NULL);
}


static void
file_save_as_cb (GtkWidget *widget, gpointer user_data)
{
	GtkFileSelection *filesel;
	
	if ((filesel = g_object_get_data ((GObject *) user_data, "save_filesel"))) {
		gdk_window_raise (((GtkWidget *) filesel)->window);
		return;
	}
	
	filesel = (GtkFileSelection *) gtk_file_selection_new (_("Save Valgrind log..."));
	g_signal_connect (filesel->ok_button, "clicked", G_CALLBACK (save_ok_cb), user_data);
	g_object_set_data ((GObject *) user_data, "save_filesel", filesel);
	
	g_signal_connect_swapped (filesel->cancel_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 
	
	g_object_weak_ref ((GObject *) filesel, (GWeakNotify) save_notify_cb, user_data);
	
	gtk_widget_show ((GtkWidget *) filesel);
}

static void
file_save_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	const char *filename;
	
	filename = g_object_get_data ((GObject *) grind, "save_filename");
	if (filename != NULL)
		save_valgrind_log (grind, filename, 0);
	else
		file_save_as_cb (widget, user_data);
}

static void
file_quit_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
}

static void
edit_cut_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_cut ((VgToolView *) grind->view);
}

static void
edit_copy_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_copy ((VgToolView *) grind->view);
}

static void
edit_paste_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_paste ((VgToolView *) grind->view);
}

static void
edit_clear_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	vg_tool_view_clear ((VgToolView *) grind->view);
}

static void
edit_prefs_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	gtk_widget_show (grind->prefs);
}

static void
edit_rules_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	vg_tool_view_show_rules ((VgToolView *) grind->view);
}

static void
tool_selected_cb (GtkRadioAction *radio, gpointer user_data)
{
	Alleyoop *grind = (Alleyoop *) user_data;
	
	grind->tool = gtk_radio_action_get_current_value (radio);
}

static void
about_destroy (GObject *obj, GObject *deadbeef)
{
	Alleyoop *grind = (Alleyoop *) obj;
	
	grind->about = NULL;
}

static void
about_response_cb (GtkDialog *about, int response_id, gpointer user_data)
{
	if (response_id == GTK_RESPONSE_CANCEL)
		gtk_widget_destroy ((GtkWidget *) about);
}

static const char copyright[] = "Copyright 2003-2011 Jeffrey Stedfast <fejj@gnome.org>";

static const char comments[] = N_("Alleyoop is a Valgrind front-end for the GNOME environment.");

static const char *authors[] = {
	"Jeffrey Stedfast <fejj@gnome.org>",
	NULL
};

static void
help_about_cb (GtkWidget *widget, gpointer user_data)
{
	Alleyoop *grind;
	
	grind = (Alleyoop *) user_data;
	if (grind->about == NULL) {
		grind->about = gtk_about_dialog_new ();
		gtk_about_dialog_set_program_name ((GtkAboutDialog *) grind->about, "Alleyoop");
		gtk_about_dialog_set_version ((GtkAboutDialog *) grind->about, VERSION);
		gtk_about_dialog_set_copyright ((GtkAboutDialog *) grind->about, copyright);
		gtk_about_dialog_set_comments ((GtkAboutDialog *) grind->about, _(comments));
		//gtk_about_dialog_set_license_type ((GtkAboutDialog *) grind->about, GTK_LICENSE_GPL_2_0);
		gtk_about_dialog_set_website ((GtkAboutDialog *) grind->about, "http://alleyoop.sourceforge.net");
		gtk_about_dialog_set_authors ((GtkAboutDialog *) grind->about, (const char **) authors);
		gtk_about_dialog_set_translator_credits ((GtkAboutDialog *) grind->about, _("translator-credits"));
		
		g_object_weak_ref ((GObject *) grind->about, (GWeakNotify) about_destroy, grind);
		g_signal_connect (grind->about, "response", G_CALLBACK (about_response_cb), grind);
		
		gtk_widget_show (grind->about);
	}
	
	gdk_window_raise (grind->about->window);
}

/* Normal menu items */
static const GtkActionEntry normal_menu_items[] = {
	{ "FileMenu",          NULL,                         N_("_File")                  },
	{ "Run",               GTK_STOCK_EXECUTE,            N_("_Run"),
	  "<control>R",        N_("Run program"),            G_CALLBACK (file_run_cb)     },
	{ "Kill",              GTK_STOCK_CANCEL,             N_("_Kill"),
	  "<control>K",        N_("Kill program"),           G_CALLBACK (file_kill_cb)    },
	{ "Open",              GTK_STOCK_OPEN,               N_("_Open..."),
	  "<control>O",        N_("Open a log file"),        G_CALLBACK (file_open_cb)    },
	{ "Save",              GTK_STOCK_SAVE,               N_("_Save"),
	  "<control>S",        N_("Save the file"),          G_CALLBACK (file_save_cb)    },
	{ "SaveAs",            GTK_STOCK_SAVE_AS,            N_("Save _As..."),
	  "<shift><control>S", N_("Save the file as..."),    G_CALLBACK (file_save_as_cb) },
	{ "Quit",              GTK_STOCK_QUIT,               N_("_Quit"),
	  "<control>Q",        N_("Exit the program"),       G_CALLBACK (file_quit_cb)    },
	
	{ "EditMenu",          NULL,                         N_("_Edit")                  },
	{ "Cut",               GTK_STOCK_CUT,                N_("Cu_t"),
	  "<control>X",        N_("Cut to clipboard"),       G_CALLBACK (edit_cut_cb)     },
	{ "Copy",              GTK_STOCK_COPY,               N_("_Copy"),
	  "<control>C",        N_("Copy to clipboard"),      G_CALLBACK (edit_copy_cb)    },
	{ "Paste",             GTK_STOCK_PASTE,              N_("_Paste"),
	  "<control>V",        N_("Paste from clipboard"),   G_CALLBACK (edit_paste_cb)   },
	{ "Clear",             GTK_STOCK_CLEAR,              N_("C_lear"),
	  NULL,                N_("Clear log view"),         G_CALLBACK (edit_clear_cb)   },
	
	{ "SettingsMenu",      NULL,                         N_("_Settings")              },
	{ "Preferences",       GTK_STOCK_PREFERENCES,        N_("Prefere_nces"),
	  NULL,                N_("Edit preferences..."),    G_CALLBACK (edit_prefs_cb)   },
	{ "Suppressions",      NULL,                         N_("Suppressions"),
	  NULL,                N_("View/Edit Suppressions"), G_CALLBACK (edit_rules_cb)   },
	
	{ "ToolsMenu",         NULL,                         N_("_Tools")                 },
	
	{ "HelpMenu",          NULL,                         N_("_Help")                  },
	{ "About",             GTK_STOCK_ABOUT,              N_("_About"),
	  NULL,                N_("About Alleyoop"),         G_CALLBACK (help_about_cb)   },
};

static const GtkRadioActionEntry radio_menu_items[] = {
	{ "AddrCheck",  NULL, "AddrCheck", NULL, N_("Use the AddrCheck Valgrind tool"), VALGRIND_TOOL_ADDRCHECK },
	{ "MemCheck",   NULL, "MemCheck",  NULL, N_("Use the MemCheck Valgrind tool"),  VALGRIND_TOOL_MEMCHECK  },
	{ "Helgrind",   NULL, "Helgrind",  NULL, N_("Use the Helgrind tool"),           VALGRIND_TOOL_HELGRIND  },
};

static const char *menu_xml =
	"<ui>"
	"  <menubar name='MainMenu'>"
	"    <menu action='FileMenu'>"
	"      <menuitem action='Run'/>"
	"      <menuitem action='Kill'/>"
	"      <menuitem action='Open'/>"
	"      <menuitem action='Save'/>"
	"      <menuitem action='SaveAs'/>"
	"      <separator/>"
	"      <menuitem action='Quit'/>"
	"    </menu>"
	"    <menu action='EditMenu'>"
	"      <menuitem action='Cut'/>"
	"      <menuitem action='Copy'/>"
	"      <menuitem action='Paste'/>"
	"      <menuitem action='Clear'/>"
	"    </menu>"
	"    <menu action='SettingsMenu'>"
	"      <menuitem action='Preferences'/>"
	"      <menuitem action='Suppressions'/>"
	"    </menu>"
	"    <menu action='ToolsMenu'>"
	"      <menuitem action='AddrCheck'/>"
	"      <menuitem action='MemCheck'/>"
	"      <menuitem action='Helgrind'/>"
	"    </menu>"
	"    <menu action='HelpMenu'>"
	"      <menuitem action='About'/>"
	"    </menu>"
	"  </menubar>"
	"</ui>";

static GtkWidget *
alleyoop_toolbar_new (Alleyoop *grind)
{
	GtkWidget *toolbar, *image;
	GtkToolbarChild *child;
	
	toolbar = gtk_toolbar_new ();
	
	image = gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_show (image);
	gtk_toolbar_append_item ((GtkToolbar *) toolbar, _("Run"), _("Run Program"), NULL,
				 image, G_CALLBACK (file_run_cb), grind);
	
	image = gtk_image_new_from_stock (GTK_STOCK_CANCEL, GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_show (image);
	gtk_toolbar_append_item ((GtkToolbar *) toolbar, _("Kill"), _("Kill Program"), NULL,
				 image, G_CALLBACK (file_kill_cb), grind);
	
	gtk_toolbar_insert_stock ((GtkToolbar *) toolbar, GTK_STOCK_OPEN, _("Open Log File"),
				  NULL, G_CALLBACK (file_open_cb), grind, 2);
	gtk_toolbar_insert_stock ((GtkToolbar *) toolbar, GTK_STOCK_SAVE, _("Save Log File"),
				  NULL, G_CALLBACK (file_save_cb), grind, 3);
	
	gtk_widget_show (toolbar);
	
	child = ((GtkToolbar *) toolbar)->children->data;
	grind->toolbar_run = child->widget;
	
	child = ((GtkToolbar *) toolbar)->children->next->data;
	grind->toolbar_kill = child->widget;
	
	return toolbar;
}

static void
prefs_response_cb (GtkDialog *dialog, int response, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	gtk_widget_hide (grind->prefs);
}

static gboolean
prefs_delete_event (GtkWidget *widget, gpointer user_data)
{
	gtk_widget_hide (widget);
	
	return TRUE;
}

GtkWidget *
alleyoop_new (ValgrindTool tool, const char **argv, const char **srcdir)
{
	GtkWidget *menubar, *widget, *vbox;
	GtkActionGroup *actions;
	GtkAccelGroup *accels;
	GError *err = NULL;
	Alleyoop *grind;
	char *title;
	int i;
	
	grind = g_object_new (ALLEYOOP_TYPE, NULL);
	
	if (argv && argv[0] != NULL)
		title = g_strdup_printf ("Alleyoop - [%s]", argv[0]);
	else
		title = g_strdup ("Alleyoop");
	
	//gnome_app_construct ((GnomeApp *) grind, "alleyoop", title);
	gtk_window_set_title ((GtkWindow *) grind, title);
	gtk_window_set_default_size ((GtkWindow *) grind, 300, 400);
	g_free (title);
	
	grind->tool = tool;
	grind->argv = argv;
	grind->srcdir = srcdir;
	
	vbox = gtk_vbox_new (FALSE, 0);
	
	/* construct the menus */
	grind->ui = gtk_ui_manager_new ();
	actions = gtk_action_group_new ("MenuActions");
	gtk_action_group_add_actions (actions, normal_menu_items, G_N_ELEMENTS (normal_menu_items), grind);
	gtk_action_group_add_radio_actions (actions, radio_menu_items, G_N_ELEMENTS (radio_menu_items), tool,
					    G_CALLBACK (tool_selected_cb), grind);
	gtk_ui_manager_insert_action_group (grind->ui, actions, 0);
	accels = gtk_ui_manager_get_accel_group (grind->ui);
	gtk_window_add_accel_group ((GtkWindow *) grind, accels);
	
	if (!gtk_ui_manager_add_ui_from_string (grind->ui, menu_xml, -1, &err)) {
		g_message ("Building menus failed: %s", err->message);
		g_error_free (err);
		exit (EXIT_FAILURE);
	}
	
	menubar = gtk_ui_manager_get_widget (grind->ui, "/MainMenu");
	gtk_widget_show (menubar);
	
	/* pack in the menubar */
	gtk_box_pack_start (GTK_BOX (vbox), menubar, FALSE, FALSE, 0);
	
	widget = alleyoop_toolbar_new (grind);
	gtk_widget_set_sensitive (grind->toolbar_run, TRUE);
	gtk_widget_set_sensitive (grind->toolbar_kill, FALSE);
	gtk_widget_show (widget);
	
	/* pack in the toolbar */
	gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);
	
	grind->view = widget = vg_default_view_new ();
	vg_tool_view_set_argv ((VgToolView *) widget, argv);
	vg_tool_view_set_srcdir ((VgToolView *) widget, srcdir);
	gtk_widget_show (widget);
	
	/* pack in the view */
	gtk_box_pack_start (GTK_BOX (vbox), widget, TRUE, TRUE, 0);
	
	/* create the prefs dialog (we just don't display it) */
	grind->prefs = alleyoop_prefs_new ();
	g_signal_connect (grind->prefs, "response", G_CALLBACK (prefs_response_cb), grind);
	g_signal_connect (grind->prefs, "delete-event", G_CALLBACK (prefs_delete_event), grind);
	
	gtk_widget_show (vbox);
	gtk_container_add ((GtkContainer *) grind, vbox);
	
	return (GtkWidget *) grind;
}

static gboolean
io_ready_cb (GIOChannel *gio, GIOCondition condition, gpointer user_data)
{
	Alleyoop *grind = user_data;
	
	if ((condition & G_IO_IN) && vg_tool_view_step ((VgToolView *) grind->view) <= 0) {
		alleyoop_kill (grind);
		grind->watch_id = 0;
		return FALSE;
	}
	
	if (condition & G_IO_HUP) {
		alleyoop_kill (grind);
		grind->watch_id = 0;
		return FALSE;
	}
	
	return TRUE;
}

void
alleyoop_run (Alleyoop *grind, GError **err)
{
	char logfd_arg[30];
	GPtrArray *args;
	int logfd[2];
	char **argv;
	int i;
	
	if (!grind->argv || !grind->argv[0])
		return;
	
	if (grind->pid != (pid_t) -1)
		return;
	
	if (pipe (logfd) == -1)
		return;
	
	args = alleyoop_prefs_create_argv ((AlleyoopPrefs *) grind->prefs, tool_names[grind->tool]);
	
	sprintf (logfd_arg, "--log-fd=%d", logfd[1]);
	g_ptr_array_add (args, logfd_arg);
	
	g_ptr_array_add (args, "--");
	for (i = 0; grind->argv[i] != NULL; i++)
		g_ptr_array_add (args, (char *) grind->argv[i]);
	g_ptr_array_add (args, NULL);
	
	argv = (char **) args->pdata;
	
	grind->pid = process_fork (argv[0], argv, TRUE, logfd[1], NULL, NULL, NULL, err);
	if (grind->pid == (pid_t) -1) {
		close (logfd[0]);
		close (logfd[1]);
		return;
	}
	
	g_ptr_array_free (args, TRUE);
	
	close (logfd[1]);
	
	vg_tool_view_connect ((VgToolView *) grind->view, logfd[0]);
	
	grind->gio = g_io_channel_unix_new (logfd[0]);
	grind->watch_id = g_io_add_watch (grind->gio, G_IO_IN | G_IO_HUP, io_ready_cb, grind);
	
	gtk_widget_set_sensitive (grind->toolbar_run, FALSE);
	gtk_widget_set_sensitive (grind->toolbar_kill, TRUE);
}


void
alleyoop_kill (Alleyoop *grind)
{
	vg_tool_view_disconnect ((VgToolView *) grind->view);
	
	if (grind->gio) {
		g_io_channel_close (grind->gio);
		g_io_channel_unref (grind->gio);
		grind->watch_id = 0;
		grind->gio = NULL;
	}
	
	if (grind->pid != (pid_t) -1) {
		process_kill (grind->pid);
		grind->pid = (pid_t) -1;
	}
	
	gtk_widget_set_sensitive (grind->toolbar_run, TRUE);
	gtk_widget_set_sensitive (grind->toolbar_kill, FALSE);
}

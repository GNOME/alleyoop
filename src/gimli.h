/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __GIMLI_H__
#define __GIMLI_H__

#include <stdint.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

typedef struct _DwarfDebugPubnamesEntry {
	struct _DwarfDebugPubnamesEntry *next;
	uint32_t offset;
	char name[1];
} DwarfDebugPubnamesEntry;

typedef struct _DwarfDebugPubnames {
	struct _DwarfDebugPubnames *next;
	uint16_t version;
	uint16_t di_offset;
	uint16_t di_size;
	DwarfDebugPubnamesEntry *entries;
} DwarfDebugPubnames;

DwarfDebugPubnames *dwarf_debug_pubnames_decode (const unsigned char *in, size_t inlen);
void dwarf_debug_pubnames_free (DwarfDebugPubnames *pubnames);

typedef struct _DwarfDebugLines {
	struct _DwarfDebugLines *next;
	
} DwarfDebugLines;

DwarfDebugLines *dwarf_debug_lines_decode (const unsigned char *in, size_t inlen);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GIMLI_H__ */

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __ALLEYOOP_PREFS_H__
#define __ALLEYOOP_PREFS_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define ALLEYOOP_TYPE_PREFS            (alleyoop_prefs_get_type ())
#define ALLEYOOP_PREFS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ALLEYOOP_TYPE_PREFS, AlleyoopPrefs))
#define ALLEYOOP_PREFS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ALLEYOOP_TYPE_PREFS, AlleyoopPrefsClass))
#define IS_ALLEYOOP_PREFS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ALLEYOOP_TYPE_PREFS))
#define IS_ALLEYOOP_PREFS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ALLEYOOP_TYPE_PREFS))
#define ALLEYOOP_PREFS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), ALLEYOOP_TYPE_PREFS, AlleyoopPrefsClass))

typedef struct _AlleyoopPrefs AlleyoopPrefs;
typedef struct _AlleyoopPrefsClass AlleyoopPrefsClass;

struct _AlleyoopPrefs {
	GtkDialog parent_object;
	
	GtkEntry *editor;
	GtkSpinButton *numlines;
	
	GtkWidget *pages[4];
};

struct _AlleyoopPrefsClass {
	GtkDialogClass parent_class;
	
};


GType alleyoop_prefs_get_class (void);

GtkWidget *alleyoop_prefs_new (void);

GPtrArray *alleyoop_prefs_create_argv (AlleyoopPrefs *prefs, const char *tool);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ALLEYOOP_PREFS_H__ */

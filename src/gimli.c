/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "gimli.h"


#ifdef ENABLE_WARNINGS
#define w(x) x
#else
#define w(x)
#endif /* ENABLE_WARNINGS */

#define d(x)


/* docs say di_offset and di_size are uint32, but the values are
 * enormous - if we replace them with uint16's and add a 32bit dummy
 * to the end of our struct, the values are much more reasonable *and*
 * seem to actually point to valid data */
struct _Dwarf2PubnamesHeader {
	uint32_t length;
	uint16_t version;
	uint16_t di_offset;
	uint16_t di_size;
	uint32_t dummy;      /* what's here? */
};


static void
pubname_entries_free (DwarfDebugPubnamesEntry *entries)
{
	DwarfDebugPubnamesEntry *next;
	
	while (entries) {
		next = entries->next;
		free (entries);
		entries = next;
	}
}

static DwarfDebugPubnamesEntry *
decode_pubname_entries (const unsigned char *inptr, const unsigned char *inend)
{
	DwarfDebugPubnamesEntry *list, *cur, *tail;
	uint32_t offset;
	size_t len;
	
	list = NULL;
	tail = (DwarfDebugPubnamesEntry *) &list;
	
	offset = ((uint32_t *) inptr)[0];
	inptr += 4;
	while (inptr < inend) {
		d(printf ("    tuple offset:      %u\n", offset));
		d(printf ("    tuple string:      %s\n", inptr));
		
		len = strlen (inptr);
		
		if (!(cur = malloc (sizeof (DwarfDebugPubnamesEntry) + len)))
			goto exception;
		
		cur->next = NULL;
		cur->offset = offset;
		strcpy (cur->name, inptr);
		
		inptr += len + 1;
		offset = ((uint32_t *) inptr)[0];
		inptr += 4;
	}
	
	return list;
	
 exception:
	
	pubname_entries_free (list);
	
	return NULL;
}


DwarfDebugPubnames *
dwarf_debug_pubnames_decode (const unsigned char *in, size_t inlen)
{
	const register unsigned char *inptr, *nextpub;
	DwarfDebugPubnames *list, *cur, *tail;
	struct _Dwarf2PubnamesHeader *pubhdr;
	const unsigned char *inend;
	
	list = NULL;
	tail = (DwarfDebugPubnames *) &list;
	
	inptr = in;
	inend = in + inlen;
	while (inptr < inend) {
		pubhdr = (struct _Dwarf2PubnamesHeader *) inptr;
		nextpub = inptr + pubhdr->length + 4;
		inptr += 14;
		
		d(printf ("length:                %u\n", pubhdr->length));
		d(printf ("version:               %u\n", pubhdr->version));
		d(printf (".debug_info offset:    %u\n", pubhdr->debug_info_offset));
		d(printf (".debug_info size:      %u\n", pubhdr->debug_info_size));
		
		if (!(cur = malloc (sizeof (DwarfDebugPubnames))))
			goto exception;
		
		cur->next = NULL;
		cur->version = pubhdr->version;
		cur->di_offset = pubhdr->di_offset;
		cur->di_size = pubhdr->di_size;
		cur->entries = decode_pubname_entries (inptr, nextpub);
		
		inptr = nextpub;
		
		tail->next = cur;
		tail = cur;
	}
	
	return list;
	
 exception:
	
	dwarf_debug_pubnames_free (list);
	
	return NULL;
}


void
dwarf_debug_pubnames_free (DwarfDebugPubnames *pubnames)
{
	DwarfDebugPubnames *next;
	
	while (pubnames) {
		next = pubnames->next;
		pubname_entries_free (pubnames->entries);
		free (pubnames);
		pubnames = next;
	}
}


typedef struct _Dwarf2LineInfo {
	uint32_t li_length;
	uint16_t li_version;
	uint32_t li_prologue_length;
	uint8_t li_min_insn_length;
	uint8_t li_default_is_stmt;
	int8_t li_line_base;
	uint8_t li_line_range;
	uint8_t li_opcode_base;
} Dwarf2LineInfo;


DwarfDebugLines *
dwarf_debug_lines_decode (const unsigned char *in, size_t inlen)
{
	register unsigned char *inptr = (unsigned char *) in;
	unsigned char *nextli, *inend = inptr + inlen;
	DwarfDebugLines *list, *tail, *node;
	Dwarf2LineInfo *li;
	unsigned char *buf;
	register int off;
	int count = 0;
	
	/* run thru the buffer to find out how many nodes we'll need */
	while (inptr < inend) {
		li = (Dwarf2LineInfo *) inptr;
		nextli = inptr + li->li_length + 4;
		
		if (li->li_version == 2) {
			count++;
		} else {
			w(fprintf (stderr, "Unexpected Dwarf version encountered in .debug_lines: %u\n", li->li_version));
		}
		
		inptr = nextli;
	}
	
	if (count == 0)
		return NULL;
	
	if (!(buf = malloc (sizeof (DwarfDebugLines) * count)))
		return NULL;
	
	off = 0;
	list = NULL;
	tail = (DwarfDebugLines *) &list;
	
	inptr = (unsigned char *) in;
	while (inptr < inend) {
		li = (Dwarf2LineInfo *) inptr;
		nextli = inptr + li->li_length + 4;
		
		if (li->li_version == 2) {
			node = (DwarfDebugLines *) (buf + off);
			node->next = NULL;
			
			inptr += 15;
			
			/* FIXME: decode the line info */
			
			tail->next = node;
			tail = node;
		}
		
		inptr = nextli;
	}
	
	return list;
}
